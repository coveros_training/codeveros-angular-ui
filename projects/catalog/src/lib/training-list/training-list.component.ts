import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatSnackBar} from '@angular/material';

import {Training, TrainingType} from '../training.interface';
import {TrainingService} from '../training.service';
import {TrainingDialogComponent} from '../training-dialog/training-dialog.component';
import {TrainingDeleteDialogComponent} from '../training-delete-dialog/training-delete-dialog.component';

@Component({
  templateUrl: './training-list.component.html',
  styleUrls: [ './training-list.component.scss' ]
})
export class TrainingListComponent implements OnInit, OnDestroy {
  training: Training[];
  displayedColumns = [ 'name', 'description', 'duration', 'type', 'actions' ];
  loading = true;

  typeOptions = {
    presentation: 'Presentation',
    workshop: 'Workshop',
    course: 'Course'
  };

  constructor(
    private matDialog: MatDialog,
    private trainingService: TrainingService,
    private matSnackBar: MatSnackBar,
    public cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.trainingService.getAll()
      .subscribe(training => {
        this.training = training;
        this.loading = false;
      });
  }

  ngOnDestroy(): void {
  }

  openAddDialog(): void {
    const dialogRef = this.matDialog.open(TrainingDialogComponent, {
      width: '750px',
      maxWidth: '95%',
      data: {}
    });

    dialogRef.afterClosed().subscribe((newTraining: Training) => {
      if (newTraining) {
        this.training = [newTraining, ...this.training];
        this.matSnackBar.open(`Training Course: ${newTraining.name} added to catalog`);
      }
    });
  }

  editTraining(training: Training) {
    const dialogRef = this.matDialog.open(TrainingDialogComponent, {
      width: '750px',
      maxWidth: '95%',
      data: { training }
    });

    dialogRef.afterClosed().subscribe((updated: Training) => {
      if (updated) {
        const index = this.training.findIndex(t => t._id === updated._id);
        if (index > -1) {
          const currTraining = [...this.training];
          currTraining[index] = updated;
          this.training = currTraining;
        }
        this.matSnackBar.open(`Training Course: ${updated.name} updated`);
      }
    });
  }

  deleteTraining(training: Training) {
    const dialogRef = this.matDialog.open(TrainingDeleteDialogComponent, {
      data: { training }
    });

    dialogRef.afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this.trainingService.deleteTraining(training._id)
          .subscribe(deletedTraining => {
            deletedTraining = training; // TODO: remove this when deleted item gets returned in request
            this.training = this.training.filter((t: Training) => t._id !== deletedTraining._id);
            this.matSnackBar.open(`Training ${deletedTraining.name} removed from catalog`);
          });
      }
    });
  }
}
