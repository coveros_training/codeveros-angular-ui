import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';

import { TRAINING_CONFIG } from 'catalog';
import { AuthModule } from 'auth';

import { AppComponent } from './app.component';
import {SharedModule} from './shared/shared.module';
import {AppRoutingModule} from './app-routing.module';
import {CoreModule} from './core/core.module';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS} from '@angular/material';
import { environment } from '../environments/environment';
import { HomeComponent } from './home/home.component';
import {WelcomeComponent} from './welcome/welcome.component';

export function tokenGetter() {
  return localStorage.getItem('access_token');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter
      }
    }),
    CoreModule,
    SharedModule,
    AuthModule,
    AppRoutingModule
  ],
  providers: [
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 3000 } },
    {
      provide: TRAINING_CONFIG,
      useValue: {
        endpoint: environment.catalogEndpoint || 'api/training'
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
